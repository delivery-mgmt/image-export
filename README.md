This depends on crane cli, kubectl and base64 utils being installed and on the path.

https://github.com/google/go-containerregistry/tree/main/cmd/crane


## To use

- image-export.sh will bundle all images that are located in the source registry from the k8s cluster you are currently pointed at. The intent is to target certain container registries for transfer.
- run 
```
image-export.sh -e <source registry>
```
and a `/tmp/image-bundle.tar` will be created for all the related images

- move that tar over to the target system and run 
```
image-export.sh -i <bundle file> <source registrty> <target registry>
```

### Example

```bash
#Set quay.io as an example source registy
#kubectl is pointed at the source cluster I want to move images from
./image-export.sh -e quay.io

#every image that is located at quay.io is bundled into /tmp/image-bundle.tar

#image-bundle.tar is moved over to the target system
#use crane auth to login to any registries you will push to
crane auth login harbor.proto.tsfrt.net -u admin -p *****

#in order to load all the images from a certain source registrty (based on just ip or hostname)
#run the following.
#If you are using harbor, this assumes you have recreated the projects from the source registry

./image-export.sh -i /tmp/image-bundle.tar quay.io harbor.proto.tsfrt.net

#this loaded all the images from quay.io as long as there was a project in the desination harbor to support the image.  
#For registries that do not have projects, this will not be a problem.


