#!/usr/bin/env bash

set -euo pipefail

WORKING_DIR=$(dirname "$0")
IMAGE_MANIFEST=/tmp/image.tmp
IMAGE_CACHE=/tmp/image-cache
IMAGE_BUNDLE=/tmp/image-bundle.tar

validate_depends() {
	if ! command -v crane &>/dev/null; then
		echo "crane could not be found"
		exit
	fi

	if ! command -v kubectl &>/dev/null; then
		echo "kubectl could not be found"
		exit
	fi

	if ! command -v base64 &>/dev/null; then
		echo "base64 could not be found"
		exit
	fi
}

copy_image() {
	image_name=$1
	b64_image_name=$(echo "$image_name" | base64)
	crane pull "$image_name" "$IMAGE_CACHE/${b64_image_name}.tar"
	echo copied "$image_name" to "$IMAGE_CACHE/""${b64_image_name}"".tar"
}

create_image_manifest() {
	#grab all the images
	kubectl get pods --all-namespaces -o jsonpath="{.items[*].spec.containers[*].image}" | tr -s '[[:space:]]' '\n' | sort | uniq -c >"$IMAGE_MANIFEST"
}

cleanup() {

	if [[ -d $IMAGE_CACHE ]]; then
		echo "Removing $IMAGE_CACHE"
		rm -rf $IMAGE_CACHE
	fi

	mkdir -p "$IMAGE_CACHE"
}

bundle_images() {

	cleanup
	create_image_manifest

	while read line; do
		image_name="$(echo "$line" | cut -d" " -f 2)"
		if from_source "$image_name"; then
			copy_image "$image_name"
		fi
	done <"$IMAGE_MANIFEST"

	tar -cvf "$IMAGE_BUNDLE" "$IMAGE_CACHE"/*.tar
	echo "created $IMAGE_BUNDLE"
	cleanup
}

from_source() {
	image=$1
	if [[ ${image} =~ ^${source_registry}* ]]; then
		return 0
	else
		return 1
	fi
}

import_bundle() {
	images=/tmp/image-cache
	image_list=/tmp/image_list
	tar tvf "${bundle_file}" | tr -s ' ' | cut -d" " -f9 >$image_list
	tar xvf "${bundle_file}" -C $images
	#rm "${bundle_file}"

	while read line; do
		b64_file=$(basename "${line}")
		b64_imagename=$(echo "${b64_file%.*}")
		imagename=$(echo "$b64_imagename" | base64 -d)
		if from_source "$imagename"; then
			relocated_image=${imagename//"$source_registry"/"$target_registry"}
			crane push $images/"$line" "$relocated_image"
			echo pushed "$imagename" to "$relocated_image"

			#only deleting if loaded, may be wanted
			rm -Rf $images/"$line"
		else
			echo "image: ${imagename}\n file:(${b64_imagename})\n >>is not from source registry, will be left on file system"
		fi
	done <"$image_list"
}

key="$1"

validate_depends

case $key in
-e | --export)
	source_registry=$2
	bundle_images
	;;
-i | --import)
	bundle_file=$2
	source_registry=$3
	target_registry=$4
	import_bundle "$bundle_file"
	;;
esac
